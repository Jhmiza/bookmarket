package net.tncy.jhm;

import static org.junit.Assert.assertTrue;

import net.tncy.jhm.bookmarket.data.Book;
import net.tncy.jhm.validator.ISBNValidator;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        ISBNValidator validator = new ISBNValidator();
        Book a = new Book("ae");
        assertTrue(a.getName().equals("ae"));
        assertTrue(validator.isValid(a.getName(),null));
    }
}
