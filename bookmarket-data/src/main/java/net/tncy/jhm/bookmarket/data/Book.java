package net.tncy.jhm.bookmarket.data;

import net.tncy.jhm.validator.ISBN;

public class Book {
    @ISBN
    private String name;
    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
