package net.tncy.jhm.bookmarket.data;

import java.util.ArrayList;
import java.util.List;

public class Bookstore {
    private String name;
    private List<Book> books = new ArrayList<>();

    public Bookstore(String name, List<Book> books) {
        this.name = name;
        this.books = books;
    }
    public List<Book> getBooks() {
        return books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
