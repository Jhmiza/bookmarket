package net.tncy.jhm.bookmarket.services;

import net.tncy.jhm.bookmarket.data.Book;
import net.tncy.jhm.bookmarket.data.Bookstore;

import java.util.*;
import java.util.stream.Collectors;

public class BookstoreService {
    private static final List<Book> BOOKS_1 = new ArrayList(Arrays.asList(new Book("book-1"), new Book("book-2")));
    private static final List<Bookstore> BOOKSTORE_LIST = new ArrayList<Bookstore>(Arrays.asList(new Bookstore("bookstore-1", BOOKS_1)));

    public List<Book> getAllBooks() {
        Set<Book> res = new HashSet<>();
        for(Bookstore bookstore: BOOKSTORE_LIST) {
            res.addAll(bookstore.getBooks());
        }
        return new ArrayList(res);
    }
}
